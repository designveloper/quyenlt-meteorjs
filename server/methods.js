import { Meteor } from 'meteor/meteor';
import { Players } from '../imports/api/players';

Meteor.methods({
  insertPlayer(player) {
    Players.insert(player);
  },

  editPlayer(player) {
    Players.update(player._id, {$set: player});
  },

  deletePlayer(id) {
    Players.remove(id);
  }
});
