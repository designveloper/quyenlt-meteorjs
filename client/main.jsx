import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from '../imports/ui/App';
import About from '../imports/ui/About';
import New from '../imports/ui/New';
import NotFound404 from '../imports/ui/notFound404';
import { Layout } from '../imports/ui/Layout';

injectTapEventPlugin();

Meteor.startup(() => {
  render((
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path='/' component={App}/>
          <Route exact path='/about' component={About} />
          <Route exact path='/new' component={New} />
          <Route exact path='*' component={NotFound404} />
        </Switch>
      </Layout>
    </BrowserRouter>
  ), document.getElementById('root'));
})
