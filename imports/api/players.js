import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Players = new Mongo.Collection('players');

Players.allow({
  insert() { return false; },
  update() { return false; },
  remove() { return false; }
})

Players.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; }
})

const PlayerSchema = new SimpleSchema({
  name: {type: String},
  fullName: {type: String},
  birthYear: {type: Number, defaultValue: 0},
  nation: {type: String},
  team: {type: String},
  teamRank: {type: String},
  position: {type: String},
  avatar: {type: String},
  description: {type: String},
  createdDate: {type: Date},
  dateModified: {type: Date}
});

Players.attachSchema(PlayerSchema);
