import * as React from 'react';
import Avatar from 'material-ui/Avatar';
import { ListItem } from 'material-ui/List';
import ActionDeleteForever from 'material-ui/svg-icons/action/delete-forever';
import { red500 } from 'material-ui/styles/colors';
import ActionDelete from 'material-ui/svg-icons/action/delete-forever';

export default class Team extends React.Component {

  deletePlayer(id) {
    Meteor.call('deletePlayer', id, (error) => {
      if(error) {
        alert('Oops! Something went wrong: ' + error.reason);
      } else {
        alert('Success! Player has been deleted!');
      }
    });

  }

  render() {
    return(
      <ListItem
        primaryText={this.props.player.name}
        leftAvatar={<Avatar src={this.props.player.avatar}/>}
        onClick={() => this.props.updateCurrentPlayer(this.props.player)}
        rightIcon={<ActionDelete hoverColor={red500} onClick={() => this.deletePlayer(this.props.player._id)} />}
      />
    );
  }
}
