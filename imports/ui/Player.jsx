import * as React from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import { blue200, blue900 } from 'material-ui/styles/colors';

const styles = {
  chip: {
    margin: 4,
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  button: {
    margin: 12,
  }
};

export default class Player extends React.Component {

  showEditPlayer() {
    this.props.showEditPlayer();
  }

  render() {
    return(
      <div>
        <Card>
          <CardMedia
            overlay={<CardTitle title={this.props.player.name}
              subtitle={`${this.props.player.team} - ${this.props.player.position}`} />}
          >
            <img src={this.props.player.avatar} alt="avatar" />
          </CardMedia>
          <CardText>
            <div style={styles.wrapper}>
              {this.props.player.description}
            </div>
          </CardText>
          <CardActions>
            <button className="btn waves-light waves-effect" onClick={this.showEditPlayer.bind(this)}>Edit</button>
          </CardActions>
        </Card>
      </div>
    );
  }
}
