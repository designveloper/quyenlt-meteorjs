import * as React from 'react';
import { browserHistory } from 'react-router';
import { Players } from '../api/players';

export default class New extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      currentAvatar: 'https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg'
    }

    this.changeAvatar = this.changeAvatar.bind(this);
  }

  changeAvatar(event) {
    this.setState({currentAvatar:event.target.value});
  }

  submitPlayer(event) {
    event.preventDefault();

    var player = {
      name: this.refs.ing.value,
      fullName: this.refs.fullName.value,
      birthYear: this.refs.birthYear.value,
      nation: this.refs.nation.value,
      team: this.refs.team.value,
      teamRank: this.refs.teamRank.value,
      position: this.refs.position.value,
      avatar: this.refs.avatar.value,
      description: this.refs.description.value,
      createdDate: new Date(),
    }

    // Players.insert(Player);

    Meteor.call('insertPlayer', player, (error) => {
      if(error) {
        alert('Oops! Something went wrong: ' + error.reason);
      } else {
        alert('Success! Player has been added!');
        window.location = "/";
      }
    });

  }

  render() {
    return(
      <div className="row">
        <form className="col s12" onSubmit={this.submitPlayer.bind(this)}>
          <h3>Add new Player</h3>
          <div className="row">
            <div className="input-field col s6">
              <img src={this.state.currentAvatar} alt="Avatar" width="80%"/>
            </div>
            <div className="input-field col s6">
              <input placeholder="Avatar" ref="avatar" type="text" className="validate" onChange={this.changeAvatar} />
            </div>
          </div>
          <div className="row">
            <div className="input-field col s6">
              <input placeholder="ING" ref="ing" type="text" className="validate" />
            </div>
            <div className="input-field col s6">
              <input placeholder="Full Name" ref="fullName" type="text" className="validate" />
            </div>
          </div>
          <div className="row">
            <div className="input-field col s6">
              <input placeholder="Birth Year" ref="birthYear" type="text" className="validate" />
            </div>
            <div className="input-field col s6">
              <input placeholder="Nation" ref="nation" type="text" className="validate" />
            </div>
          </div>
          <div className="row">
            <div className="input-field col s6">
              <input placeholder="Team" ref="team" type="text" className="validate" />
            </div>
            <div className="input-field col s6">
              <input placeholder="Team Rank" ref="teamRank" type="text" className="validate" />
            </div>
          </div>
          <div className="row">
            <div className="input-field col s6">
              <h5>Position</h5>
              <select className="browser-default" ref="position">
                <option value="Marksman">Marksman</option>
                <option value="Jungler">Jungler</option>
                <option value="Mid Laner">Mid Laner</option>
                <option value="Support">Support</option>
                <option value="Tank">Tank</option>
              </select>
            </div>
            <div className="input-field col s6">
              <textarea placeholder="Description" rows={5} ref="description" type="text" className="materialize-textarea"></textarea>
            </div>
          </div>
          <div className="row">
            <div className="col s6">
              <button className="btn waves-effect waves-light" type="reset">
                Reset
              </button>
            </div>
            <div className="col s6">
              <button className="btn waves-effect waves-light" type="submit" name="action">
                Submit<i className="material-icons right">send</i>
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
