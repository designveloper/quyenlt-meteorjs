import * as React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router-dom';
import AccountsWrapper from './AccountsWrapper';

export class Layout extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false
    }
  }

  handleToggle = () => this.setState({open: !this.state.open});

  render() {
    return(
      <MuiThemeProvider>
        <div className="container">
          <AppBar title="MeteorJS Application"
                  iconClassNameRight="muidocs-icon-navigation-expand-more"
                  showMenuIconButton={true}
                  onLeftIconButtonClick={this.handleToggle}>
                    <AccountsWrapper />
                  </AppBar>
                  <Drawer open={this.state.open} docked={false} onRequestChange={(open) => this.setState({open})}>
                    <Link to="/"><MenuItem onClick={this.handleToggle}>Home</MenuItem></Link>
                    <Link to="/about"><MenuItem onClick={this.handleToggle}>About Me</MenuItem></Link>
                  </Drawer>
          <div className="col s12">
            {this.props.children}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}
