import * as React from 'react';

export default class NotFound404 extends React.Component {
  render() {
    return(
      <h3>This page is not available</h3>
    );
  }
}
