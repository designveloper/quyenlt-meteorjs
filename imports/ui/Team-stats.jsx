import * as React from 'react';
import {Radar} from 'react-chartjs-2';

const data = {
  labels: ['Attack', 'Support', 'Defence', 'Control Game', 'Speed', 'Kills', 'Deads'],
  datasets: [
    {
      label: 'Team Stats',
      backgroundColor: 'rgba(179,181,198,0.2)',
      borderColor: 'rgba(179,181,198,1)',
      pointBackgroundColor: 'rgba(179,181,198,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(179,181,198,1)',
      data: [92, 75, 85, 80, 72, 60, 39]
    }
  ]
};

export default class TeamStats extends React.Component{
  render() {
    return (
      <div>
        <h3>Team Stats</h3>
        <Radar data={data} width={500} height={500} />
      </div>
    );
  }
}
