import * as React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import List from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router-dom';
import { createContainer } from 'meteor/react-meteor-data';

import { Players } from '../api/players';

import Team from './Team';
import TeamStats from './Team-stats';
import Player from './Player';
import AccountsWrapper from './AccountsWrapper';
import Edit from './Edit';

export class App extends React.Component {

  constructor(props) {
    super(props);

    var tempPlayer = {
      name: "Temp Player",
      fullName: "Temporary Player",
      birthYear: 2018,
      nation: "Viet Nam",
      team: "NULL",
      teamRank: "TBA",
      position: "1",
      avatar: "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg",
      description: "This is a temporary player",
    }

    this.state = {
      currentPlayer: tempPlayer,
      showEditPlayer: false,
      open: false
    }

    this.updateCurrentPlayer = this.updateCurrentPlayer.bind(this);
    this.showEditPlayer = this.showEditPlayer.bind(this);
    this.showTeamStats = this.showTeamStats.bind(this);
  }

  handleToggle = () => this.setState({open: !this.state.open});

  componentDidMount() {

    this.setState({
      players: Players.find({}).fetch()
    })
  }

  updateCurrentPlayer(player) {
    this.setState({
      currentPlayer: player
    })
  }

  showForm() {
    if(this.state.showEditPlayer === true) {
      return <Edit currentPlayer={this.state.currentPlayer}
                   showTeamStats={this.showTeamStats}
                   updateCurrentPlayer={this.updateCurrentPlayer} />
    } else {
      return <TeamStats />
    }
  }

  renderPlayer() {
    return this.props.players.map((player, index) => {
      return(
        <Team key={index} player={player} updateCurrentPlayer={this.updateCurrentPlayer} />
      )
    })
  }

  showEditPlayer() {
    this.setState({
      showEditPlayer: true
    })
  }

  showTeamStats() {
    this.setState({
      showEditPlayer: false
    })
  }

  render() {
    return(
      <div className="row">
        <div className="col s12 m7">
          <Player player={this.state.currentPlayer} showEditPlayer={this.showEditPlayer}/>
        </div>
        <div className="col s12 m5">
          <h3>Team List</h3>
          <Link to="/new" className="waves-effect btn waves-light">Add Player</Link>
          <Divider />
          <List>
            {this.renderPlayer()}
          </List>
        </div>
        <div className="col s12 m5">
          {this.showForm()}
        </div>
      </div>
    );
  }
}

export default createContainer(() => {
  Meteor.subscribe('players');
  return {
    players: Players.find({}).fetch()
  }
}, App);
