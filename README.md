# SUMMARY #

### 1. Overview ###

* The course about Meteor JS

### 2. Detail ###

React is a Javascript Framework that help builds big and fast web applications
In React, whenever there is an event that changes the state, React only renderers the components that need refreshing and not the entire DOM or the web page
In React, there are tow types of components: Stateful and Stateless.
	- Stateless: If component doesn't do much on its own and need a parent component to do something
Props: The tool is allows us to pass data from state to stateless components [But it only use to pass data from the state parent component to the children and not the other way around]

** Meteor will rebuild and restart the server and client when makes any changes on the client

To create app
	Meteor create %APP_NAME%

To run the app
	Meteor
	
Install npm packages and save it to package.json
	Meteor npm install --save %PACKAGE_NAME%
	
Material-UI: meteor npm install --save material-ui

### 3. Conclusion ###

* Meet a lot of errors when doing project because different version. 